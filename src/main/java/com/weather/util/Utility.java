package com.weather.util;

import static io.restassured.RestAssured.given;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import org.json.JSONObject;
import com.google.gson.Gson;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * This utility file consists of methods for interacting and parsing response from the API.
 * @author karan
 *
 */
public class Utility {

	private static JsonPath jpath;
	public static RequestSpecification createRequest(String lon, String lat, String key) {
		return given().
				params("lat",""+lat+"","lon",""+lon+"","key",""+key+"");
	}

	public static Response submitRequest(String url, RequestSpecification request) {
		return request.when().
				get(url);
	}
	
	public static void parseResponseCodeValueByKey(String key,Response response) {
		System.out.println(key.split("[.]")[1]+" - Value :" +jpath.get(key));
	}

	public static RequestSpecification createRequest(String postalCode, String key) {
		return given().
				params("postal_code",""+postalCode+"","key",""+key+"");
	}

	public static void parseResponse(Response response) {
		 jpath=response.jsonPath();
	}

	public static void parseArrayResponseValueByKey(String key, Response response) {
		String ArrResponseData1=null;
		ArrayList<String> ArrResponseData=jpath.get(key);
		if(ArrResponseData.toString().contains("[")) {
		ArrResponseData1=ArrResponseData.toString().substring(1,ArrResponseData.toString().length());
		ArrResponseData1=ArrResponseData1.toString().substring(0,ArrResponseData1.length()-1).replace("=", ":");
		}else {
			ArrResponseData1=ArrResponseData.toString();
		}
		JSONObject jsonResponseObj=new JSONObject(ArrResponseData1);
		HashMap<String, Object> jsonResponseMap = new Gson().fromJson(jsonResponseObj.toString(), HashMap.class);
		for (Entry<String, Object> e:jsonResponseMap.entrySet()) {
			System.out.println(key +e.getKey()+ " Value: "+e.getValue());
		}
	}
    
}
