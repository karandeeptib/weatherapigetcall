package com.weather.stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import com.weather.util.Utility;

/**
 * This is the step definition file for the features defined in WeatherGetAPI.features 
 * @author karan
 *
 */
public class GETWeatherDataSteps {

	public static Response response;
	public static RequestSpecification request;


	@Given("I want to execute Get call for weatherAPI at longitude (.*) and latitude (.*) and Auth Key (.*)")
	public void i_want_to_execute_Get_call_for_weatherAPI(String lon, String lat, String key) {
		System.out.println("Running Scenarios to get the value of /data/state_code");
		request = Utility.createRequest(lon, lat, key);
	}

	@When("I submit the request for weather details at WeatherAPI URL (.*)")
	public void i_submit_the_request_for_weather_details_at_WeatherAPI_URL(String url) {
		response = Utility.submitRequest(url, request);
	}

	@Then("I should get the state code")
	public void i_should_get_the_value_of_state_code() {
		Utility.parseResponse(response);
		Utility.parseResponseCodeValueByKey("data.state_code", response);
	}

	@Given("I want to execute Get call for weatherAPI at postal code (.*) with Auth Key (.*)")
	public void i_want_to_execute_Get_call_for_weatherAPI_at_postal_code(String postalCode, String key) {
		System.out.println("Running Scenarios to get the value of timestamp and weather details");
		request = Utility.createRequest(postalCode, key);
	}

	@Then("I should get the weather details")
	public void i_should_get_the_weather_details_along_with_timestamp() {
		Utility.parseResponse(response);
		Utility.parseResponseCodeValueByKey("data.datetime",response);
		Utility.parseArrayResponseValueByKey("data.weather",response);
	}

}
