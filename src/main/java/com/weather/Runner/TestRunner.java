package com.weather.Runner;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources/WeatherGetAPI.feature", glue = "com.weather.stepDefinitions", monochrome = true, dryRun = false, plugin = {
		"json:json_output/cucumber.json", "junit:junit_xml/cucumber.xml" })

/**
 * TestRunner class used to execute the Cucumber test
 * @author karan
 *
 */
public class TestRunner {
}
