Feature: Fetching Weather information

Scenario Outline: Get Weather data for the city based on lat and lon

Given I want to execute Get call for weatherAPI at longitude <lon> and latitude <lat> and Auth Key <key>
When I submit the request for weather details at WeatherAPI URL <url>
Then I should get the state code
Examples:
|url|lon|lat|key|
|https://api.weatherbit.io/v2.0/current|-73.935242|40.730610|f9df5f62b82442bb9521f749a401b59b|


Scenario Outline: Get weather data for the city based on postal code

Given I want to execute Get call for weatherAPI at postal code <postalCode> with Auth Key <key>
When I submit the request for weather details at WeatherAPI URL <url>
Then I should get the weather details
Examples:
|postalCode|url|key|
|28546|https://api.weatherbit.io/v2.0/current|f9df5f62b82442bb9521f749a401b59b|
