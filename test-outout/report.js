$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/resources/WeatherGetAPI.feature");
formatter.feature({
  "line": 1,
  "name": "Fetching Weather information",
  "description": "",
  "id": "fetching-weather-information",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Validating data for the city based on lat and lon",
  "description": "",
  "id": "fetching-weather-information;validating-data-for-the-city-based-on-lat-and-lon",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I want to execute Get call for weatherAPI at longitude \u003clon\u003e and latitude \u003clat\u003e and Auth Key \u003ckey\u003e",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I submit the request for weather details at WeatherAPI URL \u003curl\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should get the value of state code as \u003cstateCode\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "fetching-weather-information;validating-data-for-the-city-based-on-lat-and-lon;",
  "rows": [
    {
      "cells": [
        "url",
        "lon",
        "lat",
        "key",
        "stateCode"
      ],
      "line": 10,
      "id": "fetching-weather-information;validating-data-for-the-city-based-on-lat-and-lon;;1"
    },
    {
      "cells": [
        "https://api.weatherbit.io/v2.0/current",
        "-73.935242",
        "40.730610",
        "f9df5f62b82442bb9521f749a401b59b",
        "NY"
      ],
      "line": 11,
      "id": "fetching-weather-information;validating-data-for-the-city-based-on-lat-and-lon;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Validating data for the city based on lat and lon",
  "description": "",
  "id": "fetching-weather-information;validating-data-for-the-city-based-on-lat-and-lon;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I want to execute Get call for weatherAPI at longitude -73.935242 and latitude 40.730610 and Auth Key f9df5f62b82442bb9521f749a401b59b",
  "matchedColumns": [
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I submit the request for weather details at WeatherAPI URL https://api.weatherbit.io/v2.0/current",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should get the value of state code as NY",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "-73.935242",
      "offset": 55
    },
    {
      "val": "40.730610",
      "offset": 79
    },
    {
      "val": "f9df5f62b82442bb9521f749a401b59b",
      "offset": 102
    }
  ],
  "location": "GETWeatherDataSteps.i_want_to_execute_Get_call_for_weatherAPI(String,String,String)"
});
formatter.result({
  "duration": 1475991800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://api.weatherbit.io/v2.0/current",
      "offset": 59
    }
  ],
  "location": "GETWeatherDataSteps.i_submit_the_request_for_weather_details_at_WeatherAPI_URL(String)"
});
formatter.result({
  "duration": 4032550900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NY",
      "offset": 40
    }
  ],
  "location": "GETWeatherDataSteps.i_should_get_the_value_of_state_code(String)"
});
formatter.result({
  "duration": 637752300,
  "status": "passed"
});
});