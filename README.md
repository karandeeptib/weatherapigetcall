# README #

This is the project for fetching and parsing the response from Weather bit api based on longitutde, latitude and postal code inputs

### Tools/Framework/Libraries ###

- Maven - build tool
- Junit - test runner
- Cucumber - BDD/Gherkin style feature files
- Rest assured - Rest api library

### How to run the test ###

- Clone the project into local directory
- Import project into your workspace
- Run the Junit test from the TestRunner file of the cucumber test

### How the project works ###

- All the test cases are defined in the feature file in the form of Given, When, Then scenarios in src/main/resources
- The data input is provided in the Examples sections of the given scenario
- A step definition file provides the implementation of test steps written in the feture file in src/main/java
- Step definition interacts with the Utility class which has the implementation logic to fetch and parse the JSON Response.
